//MEU QUERIDO CODIGO
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ESTUTURA QUE SERA USADA PARA LISTA
typedef struct pessoas
{
	int conteudo;
	struct pessoa *prox;
}pessoa;

typedef struct lista
{
	pessoa *inicio;
}lista;

//FUNCOES QUE IREI USAR
int menu();
void funcao(lista *li, int opcao);
void inserir(lista *li, int valor);
void exibir(lista *li);

//FUNCAO MENU PARA ESCOLHER O QUE OCARA QUER FAZER 
int menu(){
	int opcao;

	printf("== ESCOLHA O QUE DESEJA FAZER ==\n");
	printf("0. Sair\n");
	printf("1. Inserir\n");
	printf("2. Remover\n");
	printf("3. Exibir\n");
	printf("DIGITE A OPCAO DESEJADA: ");
	scanf("%d%*c", &opcao);
	return opcao;
}

//FUNCAO QUE IRA RECEBER A ESCOLHA DO USUARIO E LEVARA A FUNCAO ESCOLHIDA POR ELE
void funcao(lista *li, int opcao){
	int valor;
	switch(opcao){
		case 0:
			exit(1);
		case 1:
			printf("DIGITE O VALOR PARA ARMAZENAR: ");
			scanf("%d%*c", &valor);
			inserir(li, valor);
			break;
		case 2:
			printf("DIGITE O VALOR PARA ARMAZENAR: ");
			scanf("%d%*c", &valor);
			remover(li, valor);
			break;
		case 3:
			exibir(li);
			break;
		default:
			printf("VALOR INVALIDO!!!!!!!!!\n");
	}
}

//FUNCAO QUE INSERE OS CARA NA LISTA
void inserir(lista *li, int valor){
	if(li == NULL){
		pessoa *newNodo = (pessoa*) malloc(sizeof(pessoa));

		newNodo->conteudo = valor;
		newNodo->prox = NULL;
		li->inicio= newNodo;

	}else{
		pessoa *newNodo = (pessoa*)malloc(sizeof(pessoa));

		newNodo->conteudo = valor;
		newNodo->prox = li->inicio;
		li->inicio = newNodo;		
	}

}

void exibir(lista *li){
	pessoa *p;
	if (li->inicio == NULL)
	{
		printf("NAO TENHO NADA AQUI....\n");
	}else{
		p = li->inicio;
		while(p != NULL){
			printf("%d\n", p->conteudo);
			p = p->prox;
		}
	}
}

//REMOVER DA LISTA
void remover(lista *li, int remove){
	int sair = 0;
	pessoa *atual;
	pessoa *anterior;

	atual = malloc(sizeof(pessoa));
	anterior = malloc(sizeof(pessoa));

	atual = li->inicio;

	while(sair != 1){
		if (atual->conteudo == remove)
		{	
			if (atual == li->inicio){ //NESSE CASO SE ESTA REMOVENDO NO INICIO DA LISTA
				li->inicio = atual->prox;
				free(atual->conteudo);
				free(atual->prox);		
				sair = 1;	
			}
			//PASSOU PARA CA SIGNIFICA QUE NAO ESTA NI INICIO DA LISTA
			anterior->prox = atual->prox;
			free(atual->conteudo);
			free(atual->prox);
			sair = 1;
		}else{

			anterior = atual;
			atual= atual->prox;
			sair = 0;
		}
	}
}


//INICIO DA MAIN AQUI
int main(){
	//DECLARO UMA VARIAVEL PARA PODER ACESSAR A POSICÃO DA STRUCT QUE DESEJO
	lista *li;
	li = malloc(sizeof(lista));
	int opcao; //APENAS RECEBERA APENAS A ESCOLHA DO USUARIO PARA LEVAR PARA O SWTCH CASE

	
	do{
		opcao = menu();
		funcao(li, opcao);
	}while(opcao != 0);


	// scanf("%d", p->conteudo);
}

