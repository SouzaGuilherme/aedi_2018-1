#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//STRUCT USADA
typedef struct pessoas cad;
//FUNCAOES UTILIZADAS
void inicia(cad *LISTA);
int menu();
void libera(cad *LISTA);
int vazia(cad *LISTA);
void exibe(cad *LISTA);


//STRUCT GERAL
typedef struct pessoas
{
	void *conteudo;
	struct pessoas *prox;
}cad;

//FUNCAO QUE INICIA A LISTA
void inicia(cad *LISTA){
	LISTA->prox = NULL;
}

//FUNCAO MENU
int menu(){
	int opt;

	printf("=== ESCOLHA SUA OPÇÃO === \n");
	printf("0. Sair\n");
	printf("1. Exibir lista\n");
	printf("2. Adicionar no inicio\n");
	printf("3. Adicionar no final\n");
	printf("4. Zerar lista\n");
	printf("SUA OPÇÃO: \n");
	scanf("%d", &opt);
	return opt;
}

//FUNCAO QUE ENCAMINHA O USUARIO A PARTIR DE SUA ESCOLHA PARA FUNCAO CERTA
void opcao(cad *LISTA, int opt){
	switch(opt){
		case 0:
			libera(LISTA);
			break;
		case 1:

		case 2:

		case 3:

		case 4:

		default:
			printf("COMANDO INVALIDO....\n");
	}
} 

//TENHO QUE DESCOBRIR APRA QUE É ISSO
int vazia(cad *LISTA){
	if (LISTA->prox == NULL){
		return 1;
	}else{
		return 0;
	}
}

//FUNCAO QUE LIBERA LISTA
void libera(cad *LISTA){
	if (!vazia(LISTA)){
		cad *proxCad, *atual;
		atual = LISTA->prox;
		while(atual != NULL){
			proxCad = atual->prox;
			free(atual);
			atual = proxCad;
		}
	}
}

//FUNCAO QUE EXIBE A LISTA
void exibe(cad *LISTA){
	if (vazia(LISTA)){
		printf("LISTA VAZIA :( \n");
		return;
	}
	cad *temp;
	temp = LISTA->prox;
	while(temp != NULL){
		printf("%5d", temp->num);
		temp = temp->prox;
	}
	printf("\n\n");
}

int main(){
	cad *LISTA = (cad *) malloc(sizeof(cad));
	if(LISTA == NULL){
		printf("ERRO AO ALOCAR MEMORIA :( \n");
	}

	inicia(LISTA); //CHAMO A FUNCAO QUE INICIA A LISTA.
	// DECLARO VARIAVEL PARA CHAMAR O MENU
	int opt;
	opt = menu();
	//CHAMO A FUNCAO QUE ENCAMINHARA A ESCOLHA DO USUARIO PARA DEVIDA FUNÇÃO
	opcao(LISTA, opt);
}